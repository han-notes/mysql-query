
-- Putty shortcut
"C:\Program Files\PuTTY\putty.exe" ubuntu@13.208.212.172 -i "C:\Users\Admin\Downloads\apf-kp-db.ppk"


/*
    INSTALING LAMP

    Step 1: sudo apt-get update
    Step 2: sudo apt-get install mysql-server mysql-client libmysqlclient-dev
    Step 3: sudo apt-get install apache2 apache2-doc apache2-npm-prefork apache2-utils libexpat1 ssl-cert
    Step 4: sudo apt-get install libapache2-mod-php7.0 php7.0 php7.0-common php7.0-curl php7.0-dev php7.0-gd php-pear php-imagick php7.0-mcrypt php7.0-mysql php7.0-ps php7.0-xsl
    Step 5: sudo apt-get install phpmyadmin
*/

sudo apt update
sudo apt install apache2
sudo ufw app list
sudo ufw app info "Apache Full"
sudo ufw allow "Apache Full"
sudo apt install mysql-server
sudo apt install php libapache2-mod-php php-mysql
sudo nano /etc/apache2/mods-enabled/dir.conf
sudo systemctl restart apache2
sudo systemctl status apache2

-- OPTIONAL
apt search php- | less
apt show package_name
apt show php-cli
sudo apt install php-cli
sudo apt install package1 package2 ...


/*DELIMITER $$
CREATE PROCEDURE proc_insert_multiple_records(nofrecords numeric)
BEGIN
  DECLARE i INT DEFAULT 0;
  DECLARE nameToInsert varchar(100);
  WHILE i < nofrecords DO
  Select contact('Random Name #', i) into nameToInsert;
  INSERT INTO users(user_id, email, password, full_name, number) VALUES ('1', 'sample@mail.com', '123123', '0920123123');
  SET i = i + 1;
  END WHILE;
END$$

DELIMETER ;*/

--CREATING TABLE
CALL proc_insert_multiple_records3();

CREATE TABLE my_table1 (
  id int NOT NULL AUTO_INCREMENT,
  name varchar(100) NOT NULL,
  age int(100) NOT NULL, 
  PRIMARY KEY (`id`)
);



/*To Add Column*/
ALTER TABLE users
ADD COLUMN id INTde AUTO_INCREMENT NOT NULL PRIMARY KEY;


CREATE DATABASE DB1;
CREATE DATABASE DB2;
CREATE DATABASE DB3;
CREATE DATABASE DB4;
CREATE DATABASE DB5;


CREATE TABLE users(
  id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
  email varchar(30) not null,
  password varchar(16) not null,
  full_name varchar(30) not null,
  number varchar(30) not null
);

CREATE TABLE employees(
  id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
  first_name varchar(30) not null,
  last_name varchar(30) not null,
  email varchar(30) not null

);

CREATE TABLE customer(
  id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
  order_number varchar(30) not null,
  email varchar(30) not null,
  phone varchar(30) not null
);

CREATE TABLE movie(
  id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
  movie_name varchar(30) not null,
  genre varchar(30) not null
);

CREATE TABLE music(
  id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
  music_name varchar(30) not null,
  artist varchar(30) not null,
  genre varchar(30) not null
);



/*
    CREATING A USER AND GRANTING PERMISSION

    Step 1: sudo mysql
    Step 2: mysql -u root -p
    Step 3: CREATE USER 'username'@'host' IDENTIFIED BY 'password';
    Step 4: CREATE USER 'sammy'@'localhost' IDENTIFIED BY 'password';
    Step 5: GRANT ALL PRIVILEGE ON *.* TO 'username'@'localhost'; (office)
            (GRANT PRIVILEGE ON database.table TO 'username'@'host';)
*/

/*Query For Adding Multiple Data*/
DELIMITER $$
CREATE PROCEDURE proc_insert_multiple_records_users(nofrecords numeric)
BEGIN
  DECLARE i INT DEFAULT 0;
  DECLARE nameToInsert varchar(100);
  WHILE i < nofRecords DO
  Select concat('Random user', i) into nameToInsert;
  INSERT INTO users(email, password, full_name, number) VALUES ('Edsel', 'Rock', nameToInsert, 404);
  SET i = i + 1;
  END WHILE;
END$$

DELIMETER ;

CALL proc_insert_multiple_records_users();


/*USER*/
CREATE USER 'edsel1'@'localhost' IDENTIFIED BY 'password';
CREATE USER 'leo1'@'localhost' IDENTIFIED BY 'password';
CREATE USER 'jacob1'@'localhost' IDENTIFIED BY 'password';
CREATE USER 'john1'@'localhost' IDENTIFIED BY 'password';
CREATE USER 'rev1'@'localhost' IDENTIFIED BY 'password';
CREATE USER 'rocel1'@'localhost' IDENTIFIED BY 'password';
CREATE USER 'paulo1'@'localhost' IDENTIFIED BY 'password';
CREATE USER 'rodney1'@'localhost' IDENTIFIED BY 'password';
CREATE USER 'jerry1'@'localhost' IDENTIFIED BY 'password';

/*PERMISSION*/
GRANT INSERT, SELECT  ON demo_sample1.* TO 'leo1'@'localhost';
GRANT ALTER  ON demo_sample2.* TO 'jacob1'@'localhost';
GRANT DROP  ON demo_sample3.* TO 'john1'@'localhost';
GRANT INSERT  ON demo_sample4.* TO 'rev1'@'localhost';
GRANT UPDATE  ON demo_sample5.* TO 'rocel1'@'localhost';
GRANT DELETE, DROP  ON demo_sample1.customer TO 'paulo1'@'localhost';
GRANT SELECT  ON demo_sample2.users TO 'rodney1'@'localhost';
GRANT DROP, DELETE  ON demo_sample1.music TO 'jerry1'@'localhost';

GRANT ALL PRIVILEGES ON *.* TO 'edsel1'@'localhost';

/*
    TO EXTRACT DATA TO .sql

    Step 1: Exit mysql
    Step 2: cd "var/lib/mysql-files"
    Step 3: mysqldump -u root -p demo_sample1 > demo_sample1.sql 
             (mysqldump -uUSERNAME -p DB_NAME > exported.sql)
*/
-- Syntax:
mysqldump -u root -p demo1 > demo1.sql 
          


/*

    TO BACKUP 

    Step 1: Go to root folder
    Step 2: mysql -u root -p edsel_backup < test_1_backup.sql
*/
-- Syntax:
mysql -u root -p DB1_backup < DB1.sql



/*
    CONNECTING SSH

    Step 1: ssh 
    Step 2: sudo apt-get install openssh-client
    Step 3: ssh localhost (if connection refuse, enter step4)
    Step 4: sudo apt-get install openssh-server ii.
    Step 5: sudo service ssh status (should be running state)
    Step 6: sudo nano /etc/ssh/sshd_config (PermitRootLogin yes, PasswordAuthentication yes,)
    Step 7: sudo passwd
    Step 8: ssh <username>@<ip>
    Step 9: scp "db to copy" root@<Public IP address or hostname>:/home/ubuntu 
    Step 10: mysql -u root -p edsel_backup_sample < test_1_backup.sql
*/
-- to restart
sudo systemctl restart ssh


file transfer
  scp "test_1_backup.sql" root@172.31.3.119:/home/ubuntu
--scp db root@<IP address or hostname>:/home/ubuntu 

/*
  
*/
SELECT 'id', 'email', 'password', 'full_name', 'number'
UNION ALL
SELECT * FROM users
INTO OUTFILE '/var/lib/mysql-files/users.csv' FIELDS TERMINATED BY ','ENCLOSED BY '"'LINES TERMINATED BY '\n';