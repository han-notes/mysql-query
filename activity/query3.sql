13.126.56.116/phpmyadmin

demodb
*BJ>z+27hL$RmceY



-- SEATWORK



-- All user with full name, phone, fb, current points above 100 and below 1000

1. SELECT id, CONCAT(first_name, " ", middle_name, " ", last_name, " ") AS Fullname, phone, facebook, current_points 
	FROM users	
	WHERE current_points >= 100  AND current_points < 1000; 

-------------------------------------------------------------------------------------------------------------------------

-- Count of  disticnt user with currents points 1000 and above
2. SELECT COUNT(DISTINCT id)
	FROM users
	WHERE current_points >= 1000;

-------------------------------------------------------------------------------------------------------------------------

/*
	Count of meron bet with win status for date of 2022-01-02
	Count of wala bet with lost status for date of 2022-01-02
	Count of draw for date of 2022-01-02
*/
3. SELECT (SELECT COUNT(side) AS meron_side
FROM bets_logs
WHERE status = 'win' AND side = 'meron' AND  date_processed BETWEEN '2022-01-02 00:00:00' AND '2022-01-02 23:59:59') AS WIN,

(SELECT COUNT(side) AS wala_side
FROM bets_logs
WHERE status = 'lost' AND side = 'wala' AND date_processed BETWEEN '2022-01-02 00:00:00' AND '2022-01-02 23:59:59') AS LOST,

(SELECT COUNT(status) AS draw_status
FROM bets_logs
WHERE status = 'draw' AND date_processed BETWEEN '2022-01-02 00:00:00' AND '2022-01-02 23:59:59') AS DRAW 



/*SELECT COUNT(side), side FROM users
	JOIN bets_logs ON users.id = bets_logs.user_id
	WHERE side = "meron" AND status = "win"
    OR side = "wala" AND status ="lost"
    OR side = "draw" OR date_processed = 2022-01-02
    GROUP BY side;*/

 -------------------------------------------------------------------------------------------------------------------------


-- All user with draw bets for 2022-01-02
4. SELECT users.username, CONCAT(first_name, " ", middle_name, " ", last_name, " ") AS Fullname, status, date_processed
	FROM users
	JOIN bets_logs ON bets_logs.user_id = users.id
		WHERE status = "draw" OR date_processed = 2022-01-02;

-------------------------------------------------------------------------------------------------------------------------

-- All bets with above 1000 and less than 50000  
5. SELECT * amount  
	FROM bets_logs
	WHERE amount < 1000 AND amount > 50000;

-------------------------------------------------------------------------------------------------------------------------

-- All verfied user with currents points above 1000, mobile_verified =1
6. SELECT id, CONCAT(first_name, " ", middle_name, " ", last_name, " ") AS Fullname, current_points, mobile_verified
	FROM users
	WHERE current_points > 1000 AND mobile_verified = 0;

-------------------------------------------------------------------------------------------------------------------------

-- All username starting from d to e
7. SELECT id, username 
	FROM users 
	WHERE username RLIKE '^[de]';

-------------------------------------------------------------------------------------------------------------------------

-- All bets of user with id of 3158401, 2846581, 3304219
8. SELECT user_id, amount
	FROM bets_logs 
	WHERE user_id IN (3158401, 2846581, 3304219);

   SELECT id,amount,user_id FROM `bets_logs` 
	WHERE user_id IN ('3158401','2846581','3304219')

-------------------------------------------------------------------------------------------------------------------------

-- Count of user with  picture_in_verified
/*
1 = Verified
2 = Need to update
3 = Submitted for approval
4 = Disapproved
0 = Unprocessed
*/
9. SELECT COUNT(picture_id_verified) 
	FROM users_details 
	WHERE picture_id_verified = 1;

   SELECT COUNT(picture_id_verified) 
	FROM users_details 
	WHERE picture_id_verified = 2;

   SELECT COUNT(picture_id_verified) 
   	FROM users_details 
   	WHERE picture_id_verified = 3;

   SELECT COUNT(picture_id_verified) 
   	FROM users_details 
   	WHERE picture_id_verified = 4;
   	
   SELECT COUNT(picture_id_verified) 
   	FROM users_details 
   	WHERE picture_id_verified = 5;	

	/*SELECT 
	COUNT(CASE WHEN "1" THEN "Verified" END) AS ,
	COUNT(CASE WHEN "2" THEN "Need to update" END),
	COUNT(CASE WHEN "3" THEN "Submitted for approval" END),
	COUNT(CASE WHEN "4" THEN "Disapproved" END),
	COUNT(CASE WHEN "5" THEN "Unprocessed" END)
	FROM users_details;*/


-------------------------------------------------------------------------------------------------------------------------

-- User with user addresses with picture in verified = 1 
10. SELECT users_details.id, users_details.user_id, users_addresses.present_address, picture_id_verified 
		FROM users_details
		JOIN users_addresses ON users_details.id = users_addresses.id 
		WHERE picture_id_verified = 1;


	/*SELECT users.id, users.username, CONCAT(first_name," ",middle_name," ",last_name) AS Fullname, users_details.picture_id_verified
		FROM users
	  	LEFT JOIN users_details ON users.id = users_details.user_id
		WHERE picture_id_verified = 1;*/
	;

-------------------------------------------------------------------------------------------------------------------------


-- TASK 2

1. Users above 500 points with picture_in_verified = 1 include user details(ex. id, username, name), addresses and bday


-------------------------------------------------------------------------------------------------------------------------

2. Users above 500 points with picture_in_verified = 0 include user details(ex. id, username, name), addresses and bday


-------------------------------------------------------------------------------------------------------------------------

3. All win bet with user id username and last ip


-------------------------------------------------------------------------------------------------------------------------

4. All wala bet with user id username with amount above 1000 points


-------------------------------------------------------------------------------------------------------------------------

5. All user with picture in verified with user addresses and bday, include name and username