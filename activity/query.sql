
/*
	SELECT > FROM > LEFT JOIN > ON > WHERE
*/


-- 1
SELECT * FROM users WHERE current_points = '0'; 

-- 2
SELECT * FROM users WHERE current_points >= '100'; 

-- 3
SELECT users.username, bets_logs.status, payout, side
	FROM bets_logs
		LEFT JOIN users ON bets_logs.user_id = users.id
WHERE payout >= 500 AND payout <= 1000 AND side = 'wala'  
ORDER BY `bets_logs`.`side`  ASC

-- 4
SELECT id, username, phone, facebook, CONCAT(first_name, " ",  middle_name, " ", last_name, " ") AS Fullname FROM users;

-- 5
SELECT users.id, username, amount, status, CONCAT(first_name, " ",  middle_name, " ", last_name, " ") AS Fullname, bets_logs.arena_id
	FROM users
	    LEFT JOIN bets_logs ON bets_logs.user_id = users.id 
		WHERE arena_id = 5 and amount < 1000;