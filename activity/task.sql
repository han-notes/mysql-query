CREATE DATABASE db1;

CREATE TABLE user_type(
    id INT NOT NULL AUTO_INCREMENT,
    type varchar(255) NOT NULL,
    level INT NULL,
    PRIMARY KEY (id));


CREATE TABLE user(
    id INT NOT NULL AUTO_INCREMENT,
    username varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    user_type_id INT NOT NULL,
    status varchar(255) NULL,
    date_registered DATE,
    PRIMARY KEY (id),
    CONSTRAINT fk_user_user_type_id
        FOREIGN KEY(user_type_id) REFERENCES user_type(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT);

/**/

INSERT INTO user_type (type, level) VALUES ('superadmin', 100);
INSERT INTO user_type (type, level) VALUES ('admin', 99);
INSERT INTO user_type (type, level) VALUES ('staff', 1);
INSERT INTO user_type (type, level) VALUES ('user', 0);


INSERT INTO user (username, password, user_type_id, status, date_registered) VALUES ('root', 'qwe123', 1, 1, '2021-01-01');
INSERT INTO user (username, password, user_type_id, status, date_registered) VALUES ('admin01', 'qwe123', 2, 1, '2021-01-01');
INSERT INTO user (username, password, user_type_id, status, date_registered) VALUES ('staff01', 'qwe123', 3, 1, '2021-03-01');
INSERT INTO user (username, password, user_type_id, status, date_registered) VALUES ('staff02', 'qwe123', 3, 0, '2021-03-02');
INSERT INTO user (username, password, user_type_id, status, date_registered) VALUES ('staff03', 'qwe123', 3, 1, '2021-03-05');
INSERT INTO user (username, password, user_type_id, status, date_registered) VALUES ('user01', 'qwe123', 4, 1, '2021-04-01');
INSERT INTO user (username, password, user_type_id, status, date_registered) VALUES ('user02', 'qwe123', 4, 1, '2021-05-10');
INSERT INTO user (username, password, user_type_id, status, date_registered) VALUES ('user03', 'qwe123', 4, 1, '2021-05-24');
INSERT INTO user (username, password, user_type_id, status, date_registered) VALUES ('user04', 'qwe123', 4, 0, '2021-06-13');
INSERT INTO user (username, password, user_type_id, status, date_registered) VALUES ('user05', 'qwe123', 4, 0, '2021-06-14');



/* OUTPUT QUERY */

1
SELECT "id", "username", "user_type"
UNION ALL
SELECT user.id, user.username, user_type.type
FROM user
LEFT JOIN user_type ON user.user_type_id = user_type.id
INTO OUTFILE '/var/lib/mysql-files/all_users.csv' FIELDS TERMINATED BY ','ENCLOSED BY '"'LINES TERMINATED BY '\n';

2*
SELECT "id", "username", "user_type", "status", "date_registered"
UNION ALL
SELECT user.id, username, type, 
CASE status
WHEN '1' THEN 'Active'
WHEN '0' THEN 'Inactive'
END AS status, date_registered
FROM user
JOIN user_type ON user.user_type_id = user_type.id
WHERE user_type.level > 0
INTO OUTFILE '/var/lib/mysql-files/all_users_0.csv' FIELDS TERMINATED BY ','ENCLOSED BY '"'LINES TERMINATED BY '\n';

3
SELECT "id", "username", "user_type", "status", "date_registered"
UNION ALL
SELECT user.id, username, type, 
CASE status
WHEN '1' THEN 'Active'
WHEN '0' THEN 'Inactive'
END AS status, date_registered
FROM user
JOIN user_type ON user.user_type_id = user_type.id
WHERE status != 0
INTO OUTFILE '/var/lib/mysql-files/all_active_users.csv' FIELDS TERMINATED BY ','ENCLOSED BY '"'LINES TERMINATED BY '\n';



--------------------------------------------------------------------------------------------------------------------------------





sudo chown root:root /home
sudo chmod 755 /home
sudo chown ubuntu:ubuntu /home/ubuntu -R
sudo chmod 700 /home/ubuntu /home/ubuntu/.ssh
sudo chmod 600 /home/ubuntu/.ssh/authorized_keys







3
SELECT user.id, username, type,
CASE status
WHEN '1' THEN 'Active'
WHEN '0' THEN 'Inactive'
END AS status, date_registered
FROM user
JOIN user_type ON user.user_type_id = user_type.id
WHERE user.id <= 8 AND user.id != 4;

/* To CSV file*/
SELECT user.id, user.username, user_type.type FROM user
LEFT JOIN user_type ON user.user_type_id = user_type.id 
INTO OUTFILE '/var/lib/mysql-files/1.csv' FIELDS TERMINATED BY ','ENCLOSED BY '"'LINES TERMINATED BY '\n';


SELECT user.id, user.username, user_type.type, IF (status = 1, "active", "inactive") AS status FROM user LEFT JOIN user_type ON user.user_type_id = user_type.id WHERE user_type.level > 0
INTO OUTFILE '/var/lib/mysql-files/2.csv' FIELDS TERMINATED BY ','ENCLOSED BY '"'LINES TERMINATED BY '\n';
















SELECT user.id, user.username, user_type.type
FROM user
LEFT JOIN user_type ON user.user_type_id = user_type.id;











SELECT user.id, user.username, user.user_type,
FROM users
LEFT JOIN user_type ON user.user_type = user_type.id;